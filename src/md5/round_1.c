
#include "ft_ssl.h"

#define BUFFER(x)		md5->buffers[index[x]]

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static uint32_t	logic(uint32_t x, uint32_t y, uint32_t z)
{
	return ((x & y) | ((!x) & z));
}

static void		ope(t_md5 *md5, uint8_t *index, uint8_t offset, uint32_t add)
{
	BUFFER(0) = BUFFER(1) + ((BUFFER(0) + logic(BUFFER(1), BUFFER(2), BUFFER(3))
	+ md5->blocks[md5->active_block].t32[md5->active_word] + add) << offset);
	(md5->active_word)++;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			md5_round_1(t_md5 *md5)
{
	md5->active_word = 0;
	ope(md5, (uint8_t*)"\x01\x02\x03\x04", 7, 0xd76aa478);
}
